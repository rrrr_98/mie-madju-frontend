import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from "./router"
import store from "@/store/store";
import { CHECK_AUTH, LOAD_ITEM } from '@/store/actions.type'
import ApiService from "./common/api.service";
import { LOAD_OUT_ITEM } from './store/actions.type';
import { GET_USERS } from './store/actions.type';

Vue.config.productionTip = false

ApiService.init();

router.beforeEach(
  (to, from, next) => {
    return Promise
      .all([store.dispatch(CHECK_AUTH)])
      .then([store.dispatch(LOAD_ITEM)])
      .then([store.dispatch(LOAD_OUT_ITEM)])
      .then([store.dispatch(GET_USERS)])
      .then(next)
  }
)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
