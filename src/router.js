import Vue from "vue"
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Pemesanan from './views/Pemesanan.vue'
import Pembelian from './views/Pembelian.vue'
import Penjualan from './views/Penjualan.vue'

Vue.use(Router);

export default new Router({
  mode: "history",
  
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "login", 
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    },
    {
      path: "/pemesanan",
      name: "pemesanan",
      component: Pemesanan
    },
    {
      path: "/pembelian",
      name: "pembelian",
      component: Pembelian
    },
    {
      path: "/penjualan",
      name: "penjualan",
      component: Penjualan
    }
  ]
})