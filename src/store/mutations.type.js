export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_ERROR = "setError";
export const SET_ACCESS_TOKEN = "setAccessToken";
export const SET_ITEM = "setItem";
export const SET_OUT_ITEM = "setOutItem";
export const SET_USERS = "setUsers";