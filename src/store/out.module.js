import ApiService from "@/common/api.service";
import { LOAD_OUT_ITEM } from "./actions.type";
import { SET_OUT_ITEM } from "./mutations.type";

const state = {
  itemPenjualan: null
}

const getters = {
  itemPenjualan: state => state.itemPenjualan
};

const actions = {
  [LOAD_OUT_ITEM](context) {
    return new Promise(resolve => {
      ApiService.get("api/orders")
      .then(({data}) => {
        context.commit(SET_OUT_ITEM, data);
        resolve(data);
      })
    });
  }
};

const mutations = {
  [SET_OUT_ITEM](state, data) {
    state.itemPenjualan = data;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
};