import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth.module";
import order from './order.module';
import buy from './buy.module';
import out from './out.module';

Vue.use(Vuex);

export default new Vuex.Store({
  // state: {},
  // mutations: {},
  // actions: {},
  modules: {
    auth,
    order,
    buy,
    out
  }
});