import ApiService from "@/common/api.service";

import {
  ORDER
} from "./actions.type";

export const actions = {
  [ORDER](context, params) {
    return new Promise((resolve, reject) => {
      ApiService.post("api/orders", {
        user_id: params.idUser,
        id_barang: params.idBarang,
        jumlah: params.jumlah
      }).then(({data}) => {
        resolve(data);
      })
    });
  }
};

export default {
  actions,
};