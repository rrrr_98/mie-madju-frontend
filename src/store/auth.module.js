import ApiService from "@/common/api.service";
import TokenService from "@/common/token.service";
import { LOGIN, LOGOUT, REGISTER, CHECK_AUTH, UPDATE_USER, GET_USERS} from "./actions.type";
import { SET_AUTH, PURGE_AUTH, SET_ERROR, SET_ACCESS_TOKEN, SET_USERS} from "./mutations.type";

const state = {
  errors: null,
  user: {},
  token: {},
  isAuthenticated: !!TokenService.getToken()
};

const getters = {
  // currentUser(state) {
  //   return state.user;
  // },
  // isAuthenticated(state) {
  //   return state.isAuthenticated;
  // }
  currentUser: state => state.user,
  isAuthenticated: state => state.isAuthenticated,
  users: state => state.users,
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.post("api/auth/login", { email: credentials.username, password: credentials.password})
        .then(({ data }) => {
          context.commit(SET_ACCESS_TOKEN, data);
          resolve(data);

          return new Promise(resolve => {
            ApiService.setHeader();
            ApiService.get("api/auth/user")
              .then(({ data }) => {
                context.commit(SET_AUTH, data);
                // resolve(data);
              })
              .catch(({ response }) => {
                context.commit(SET_ERROR, response.data.errors);
              });
          });
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [GET_USERS](context) {
    let arr = [];
    ApiService.get("api/auth/users")
      .then(({ data }) => {
        data.forEach(e => {
          arr[e.id] = e.name;
        });
        context.commit(SET_USERS, arr);
      })
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [REGISTER](context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.post("api/auth/signup", { name: credentials.name, email: credentials.email, 
        password: credentials.password, password_confirmation: credentials.password_confirmation })
        .then(({ data }) => {
          context.commit(SET_AUTH, data);
          resolve(data);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    });
  },
  [CHECK_AUTH](context) {
    if (TokenService.getToken()) {
      
      return new Promise(resolve => {
        ApiService.setHeader();
        ApiService.get("api/auth/user")
          .then(({ data }) => {
            context.commit(SET_AUTH, data);
            resolve(data);
          })
          .catch(({ response }) => {
            context.commit(SET_ERROR, response.data.errors);
          });
      });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;
    state.errors = {};
  },
  [SET_USERS](state, data) {
    state.users = data;
  },
  [SET_ACCESS_TOKEN](state, data){
    TokenService.saveToken(data.access_token);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    TokenService.destroyToken();
  },
};

export default {
  state,
  actions,
  mutations,
  getters
};
