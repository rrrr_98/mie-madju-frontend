import ApiService from "@/common/api.service";
import { LOAD_ITEM } from "./actions.type";
import { SET_ITEM } from "./mutations.type";

import {
  BUY
} from "./actions.type";

const state = {
  items: null
}

const getters = {
  items: state => state.items
};

const actions = {
  [BUY](context, params) {
    return new Promise((resolve, reject) => {
      ApiService.post("api/buys", {
        user_id: params.idUser,
        id_barang: params.idBarang,
        jumlah: params.jumlah
      }).then(({data}) => {
        resolve(data);
      })
    });
  },
  [LOAD_ITEM](context) {
    return new Promise(resolve => {
      ApiService.get("api/buys")
      .then(({data}) => {
        context.commit(SET_ITEM, data);
        resolve(data);
      })
    });
  }
};

const mutations = {
  [SET_ITEM](state, data) {
    state.items = data;
  }
}

export default {
  state,
  getters,
  actions,
  mutations
};